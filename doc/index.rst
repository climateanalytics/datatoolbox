Documentation of datatoolbox
=======================================

Installation
-----------------

.. toctree::
  :maxdepth: 2
  
  installation
  
  
Getting started
-----------------

.. toctree::
   :maxdepth: 2

   first_steps
   code_structure
   
   
Tutorial
-----------------

.. toctree::
   :maxdepth: 1
    
   tutorial_data_integration
   tutorial_advanced_data_integration
   tutorial_database


Package content:
-----------------

.. toctree::
   :maxdepth: 2

   data_structures
   database
   database_gitrepomanager
   core
   tools 
   license
   help

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

