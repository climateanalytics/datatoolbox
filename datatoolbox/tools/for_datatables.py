#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 08:46:49 2020


toolbox for datatables

@author: ageiges
"""

from copy import copy

import numpy as np
import pandas as pd

# from . import config
import datatoolbox as dt

# %%
