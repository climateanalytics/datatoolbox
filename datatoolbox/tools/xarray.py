#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains all relevant tools regarding the use of xarray


@author: ageiges
"""

# from datatoolbox import core

# %%
from .. import data_structures

load_as_xdataset = data_structures.load_as_xdataset
key_set_to_xdataset = data_structures.key_set_to_xdataset
